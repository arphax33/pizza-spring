<%@page pageEncoding="UTF-8" isErrorPage="true" contentType="text/html" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Pizza Spring</title>
</head>
<body>
<a href="<c:url value="/rapport/client"/>">Nombre de commandes par client</a>
<a href="<c:url value="/rapport/pizza"/>">Pizzas vendus par catégorie</a>
<a href="<c:url value="/"/>">Acceuil</a>
</body>
</html>
