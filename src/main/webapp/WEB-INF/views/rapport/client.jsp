<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: At0m
  Date: 22/12/2019
  Time: 17:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table>
    <thead>
    <td>Nom</td>
    <td>Nombre de commandes</td>
    </thead>
    <c:forEach items="${lignes}" var="ligne">
        <tr>
            <td>${ligne.nom}</td>
            <td>${ligne.nbCommande}</td>
        </tr>
    </c:forEach>
</table>
<a href="<c:url value="/rapport"/>">Retour</a>
</body>
</html>
