<%@page pageEncoding="UTF-8" isErrorPage="true" contentType="text/html" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
  <head>
  	<meta charset="UTF-8">
    <title>Pizza Spring</title>
  </head>
  <body>
  	<a href="<c:url value="/commande/formulaire"/>">Commander</a>
    <a href="<c:url value="/commande"/>">Liste des commandes</a>
    <a href="<c:url value="/rapport"/>">Rapports d'activités</a>
  </body>
</html>