<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: At0m
  Date: 09/12/2019
  Time: 10:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Pizza Spring - Confirmation</title>
</head>
<body>
<table>
    <tr>
        <td>Nom :</td>
        <td>${nom}</td>
    </tr>
    <tr>
        <td>Téléphone :</td>
        <td>${telephone}</td>
    </tr>
    <tr>
        <td>Commandé à :</td>
        <td>${dateEmission}</td>
    </tr>
</table>
<a href="<c:url value="/"/>">Acceuil</a>
</body>
</html>
