<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: At0m
  Date: 22/12/2019
  Time: 13:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Commande suivante</title>
</head>
<body>
<c:forEach items="${commande}" var="commande">
    <label for="name">Nom :</label><p id="name"><c:out value="${commande.nom}"/></p>
    <label for="phone">Téléphone :</label><p id="phone"><c:out value="${commande.telephone}"/></p>
    <table>
        <tr>
            <th>Pizza</th>
            <th>Base</th>
            <th>Ingrédients</th>
        </tr>
        <c:forEach items="${commande.pizzas}" var="pizza">
            <tr>
            <td>${pizza.nom}</td>
            <td>${pizza.categorie.nom}</td>
            <td>
                <c:forEach items="${pizza.ingredients}" var="ingredient">
                    <p>${ingredient.nom}</p>
                </c:forEach>
            </td>
            </tr>
        </c:forEach>
    </table>
    <form:form method="post" servletRelativeAction="/commande">
        <input name="commandeId" type="hidden" value="${commande.id}">
        <button type="submit">Commande Terminée !</button>
    </form:form>
</c:forEach>
</body>
</html>
