<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: At0m
  Date: 09/12/2019
  Time: 10:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Pizza Spring - Liste des commandes</title>
</head>
<body>
    <table>
        <tr>
            <th>Nom</th>
            <th>Commandé à</th>
        </tr>

        <c:forEach var="commande" items="${commandes}">
            <tr>
                <td>${commande.nom}</td>
                <td><fmt:formatDate value="${commande.dateEmission}" type="TIME" timeStyle="LONG"/></td>
            </tr>
        </c:forEach>
    </table>

<a href="<c:url value="/processus"/>">Commande suivante</a>
</body>
</html>
