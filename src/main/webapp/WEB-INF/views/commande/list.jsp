<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: At0m
  Date: 22/12/2019
  Time: 13:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Commande à préparer</title>
</head>
<body>
<table>
    <tr>
        <th>Nom</th>
        <th>Date Emission</th>
    </tr>
    <c:forEach items="${listeCommandes}" var="commande">
        <tr>
            <td>${commande.nom}</td>
            <td>${commande.dateEmission}</td>
        </tr>
    </c:forEach>
    <a href="<c:url value="/commande/suivante"/>">Commande suivante</a>
    <a href="<c:url value="/"/>">Acceuil</a>
</table>
</body>
</html>
