<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  Created by IntelliJ IDEA.
  User: At0m
  Date: 09/12/2019
  Time: 09:42
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <title>Pizza Spring - Commande</title>
</head>
<body>
<%--@elvariable id="commandeDto" type="pizza.spring.dto.CommandeDto"--%>
<form:form method="post" servletRelativeAction="/commande/formulaire" modelAttribute="commandeDto">
    <p><label>Nom : </label><form:input path="nom"/> <form:errors path="nom"/></p>
    <p><label>Téléphone : </label><form:input path="telephone"/> <form:errors path="telephone"/></p>
    <p><label>Veuillez sélectionner la/les pizza(s) : </label>
        <form:select path="idPizzas" name="pizza">
            <form:option value="0" label="-- Pizzas --"/>
            <form:options items="${listePizzas}" itemValue="id" itemLabel="nom"/>
        </form:select>
    </p>
    <button type="submit">Envoyer</button>
</form:form>
<a href="<c:url value="/"/>">Acceuil</a>
</body>
</html>

<script>

</script>
