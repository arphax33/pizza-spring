package pizza.spring.modele;

import com.sun.istack.NotNull;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Commande {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    private String nom;
    @NotNull
    private String telephone;
    private String dateEmission;
    private Boolean enAttente;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "detailcommande",
            joinColumns = @JoinColumn(name = "commande_id"),
            inverseJoinColumns = @JoinColumn(name = "pizza_id"))
    private List<Pizza> pizzas = new ArrayList<>();

    public Commande() {
    }

    public Commande(String nom, String telephone, String dateEmission, Boolean enAttente, List<Pizza> pizzas) {
        this.nom = nom;
        this.telephone = telephone;
        this.dateEmission = dateEmission;
        this.enAttente = enAttente;
        this.pizzas = pizzas;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getDateEmission() {
        return dateEmission;
    }

    public void setDateEmission(String dateEmission) {
        this.dateEmission = dateEmission;
    }

    public Boolean getEnAttente() {
        return enAttente;
    }

    public void setEnAttente(Boolean enAttente) {
        this.enAttente = enAttente;
    }

    public List<Pizza> getPizzas() {
        return pizzas;
    }

    public void setPizzas(List<Pizza> pizzas) {
        this.pizzas = pizzas;
    }
}
