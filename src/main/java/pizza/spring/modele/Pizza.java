package pizza.spring.modele;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Pizza {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nom;
	private int prix;
	private boolean disponible;

	@ManyToOne(fetch = FetchType.EAGER)
	private Categorie categorie;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "pizza_ingredient",
			joinColumns = @JoinColumn(name = "pizza_id"),
			inverseJoinColumns = @JoinColumn(name = "ingredient_id"))
	private List<Ingredient> ingredients = new ArrayList<>();

	@ManyToMany
	@JoinTable(name = "detailcommande",
			joinColumns = @JoinColumn(name = "pizza_id"),
			inverseJoinColumns = @JoinColumn(name = "commande_id"))
	private List<Commande> commandes = new ArrayList<>();

	public Pizza() {}

	public Pizza(String nom, int prix, boolean disponible, Categorie categorie,
				 List<Ingredient> ingredients, List<Commande> commandes) {
		this.nom = nom;
		this.prix = prix;
		this.disponible = disponible;
		this.categorie = categorie;
		this.ingredients = ingredients;
		this.commandes = commandes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public boolean isDisponible() {
		return disponible;
	}

	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public List<Commande> getCommandes() {
		return commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}
}
