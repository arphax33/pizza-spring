package pizza.spring.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pizza.spring.dto.CommandeParClientDto;
import pizza.spring.modele.Categorie;
import pizza.spring.modele.Commande;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class CommandeDao {
    @PersistenceContext
    private EntityManager em;

    @Transactional(readOnly = true)
    public List<Commande> getAllCommandeEnAttente() {
        return em.createQuery("SELECT c FROM Commande c WHERE c.enAttente = true ORDER BY dateEmission DESC", Commande.class)
                .getResultList();
    }

    @Transactional
    public void commandeFinished(long id) {
        Commande commande = getFirstCommande().get(0);
        commande.setEnAttente(false);
    }

    @Transactional(readOnly = true)
    public List<Commande> getFirstCommande() {
        return em.createQuery("SELECT c FROM Commande c WHERE c.enAttente = true ORDER BY dateEmission DESC", Commande.class)
                .setMaxResults(1)
                .getResultList();
    }

    @Transactional(readOnly = true)
    public Commande findById(long id) {
        return em.createQuery("select  c from Commande c where c.id = :id", Commande.class)
                .setParameter("id", id).getSingleResult();
    }

    @Transactional
    public void save(Commande commande) {
        em.persist(commande);
    }

    @Transactional(readOnly = true)
    public List<Commande> getAll() {
        return em.createQuery("select c from Commande c", Commande.class)
                .getResultList();
    }

    @Transactional(readOnly = true)
    public List<Commande> getGroupedByName() {
        return em.createQuery("SELECT c FROM Commande c GROUP BY c.nom", Commande.class)
                .getResultList();
    }

    @Transactional(readOnly = true)
    public String countByName(String name) {
        Query query = em.createQuery("SELECT COUNT(c) FROM Commande c WHERE c.nom = :nom")
                .setParameter("nom", name);

        return query.getSingleResult().toString();
    }
}
