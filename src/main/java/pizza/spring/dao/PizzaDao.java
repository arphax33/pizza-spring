package pizza.spring.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import pizza.spring.modele.Categorie;
import pizza.spring.modele.Pizza;

@Repository
public class PizzaDao {
	@PersistenceContext
	private EntityManager em;
	
	public List<Pizza> getListePizza() {
		return em.createQuery("select p from Pizza p", Pizza.class)
				 .getResultList();
	}

	public Pizza getById(long id) {
		return em.find(Pizza.class, id);
	}

	public List<Pizza> getByCategorie(Categorie categorie) {
		return em.createQuery("SELECT p FROM Pizza p WHERE p.categorie = :categorie_id", Pizza.class)
				.setParameter("categorie_id", categorie.getId())
				.getResultList();
	}
}
