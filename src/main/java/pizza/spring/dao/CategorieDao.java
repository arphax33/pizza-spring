package pizza.spring.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pizza.spring.modele.Categorie;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class CategorieDao {
    @PersistenceContext
    private EntityManager em;

    @Transactional(readOnly = true)
    public List<Categorie> getAll() {
        return em.createQuery("SELECT c FROM Categorie c")
                .getResultList();
    }
}
