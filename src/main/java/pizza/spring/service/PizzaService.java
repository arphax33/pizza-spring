package pizza.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pizza.spring.dao.CommandeDao;
import pizza.spring.dao.PizzaDao;
import pizza.spring.modele.Categorie;
import pizza.spring.modele.Commande;
import pizza.spring.modele.Pizza;

@Service
public class PizzaService {
	@Autowired
	private PizzaDao pizzaDao;

	@Autowired
	private CommandeDao commandeDao;

@Transactional(readOnly = true)
	public List<Pizza> getListePizza() {
		return pizzaDao.getListePizza();
	}

	@Transactional
	public void save(Commande commande) {
		commandeDao .save(commande);
	}

	@Transactional(readOnly = true)
	public Pizza getById(long id) { return pizzaDao.getById(id); }

	@Transactional(readOnly = true)
	public List<Pizza> getByCategorie(Categorie categorie) { return pizzaDao.getByCategorie(categorie);}
}
