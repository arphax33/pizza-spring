package pizza.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pizza.spring.dao.CategorieDao;
import pizza.spring.modele.Categorie;

import java.util.List;

@Service
public class CategorieService {
    @Autowired
    CategorieDao categorieDao;

    public List<Categorie> getAll() {
        return categorieDao.getAll();
    }
}
