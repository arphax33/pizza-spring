package pizza.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pizza.spring.dao.CommandeDao;
import pizza.spring.dto.CommandeParClientDto;
import pizza.spring.modele.Commande;

import java.util.List;

@Service
public class CommandeService {
    @Autowired
    private CommandeDao commandeDao;

    public Commande setAsCommande(long id) {
        return commandeDao.findById(id);
    }

    public void commandeFinished(long id) { commandeDao.commandeFinished(id);}

    public List<Commande> getFirstCommande() {return commandeDao.getFirstCommande(); }

    public List<Commande> getAllCommandeEnAttente() { return commandeDao.getAllCommandeEnAttente(); }

    public void save(Commande commande) {
        commandeDao.save(commande);
    }

    public List<Commande> getAll() { return commandeDao.getAll(); }

    public List<Commande> getGroupedByName() { return commandeDao.getGroupedByName(); }

    public String countByName(String name) {
        return commandeDao.countByName(name);
    }
}
