package pizza.spring.controleur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import pizza.spring.dto.BeneficeParCategorieDto;
import pizza.spring.dto.CommandeParClientDto;
import pizza.spring.modele.Categorie;
import pizza.spring.modele.Commande;
import pizza.spring.modele.Pizza;
import pizza.spring.service.CategorieService;
import pizza.spring.service.CommandeService;
import pizza.spring.service.PizzaService;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

@Controller
public class RapportControleur {
    @Autowired
    private PizzaService pizzaService;

    @Autowired
    private CategorieService categorieService;

    @Autowired
    private CommandeService commandeService;

    @GetMapping("/rapport")
    public String rapport() {
        return "/rapport/options";
    }

    @GetMapping("rapport/client")
    public ModelAndView client(ModelMap modelMap){
        ModelAndView modelAndView = new ModelAndView("rapport/client", "commandeParClientDto",
                new ArrayList<CommandeParClientDto>());
        List<CommandeParClientDto> commandeParClientDtos = new ArrayList<>();
        List<Commande> commandes = commandeService.getGroupedByName();

        for (Commande commande: commandes) {
            CommandeParClientDto commandeParClientDto = new CommandeParClientDto();

            System.out.println(commande.getNom());
            System.out.println(commandeService.countByName(commande.getNom()));

            commandeParClientDto.setNom(commande.getNom());
            commandeParClientDto.setNbCommande(commandeService.countByName(commande.getNom()));

            commandeParClientDtos.add(commandeParClientDto);
        }

        modelMap.addAttribute("lignes", commandeParClientDtos);
        return modelAndView;
    }

    @GetMapping("rapport/pizza")
    public ModelAndView pizza(ModelMap modelMap){
        ModelAndView modelAndView = new ModelAndView("rapport/pizza", "beneficeParCategorieDto",
                new ArrayList<BeneficeParCategorieDto>());
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        List<Categorie> categories = categorieService.getAll();
        List<BeneficeParCategorieDto> beneficeParCategorieDtos = new ArrayList<>();

        for (Categorie categorie: categories){
            BeneficeParCategorieDto beneficeParCategorieDto = new BeneficeParCategorieDto();
            List<Commande> commandes = commandeService.getAll();
            int total = 0;
            int nbPizza = 0;

            for (Commande commande: commandes) {
                List<Pizza> pizzas = commande.getPizzas();
                for (Pizza pizza: pizzas) {
                    if (pizza.getCategorie().getNom().equals(categorie.getNom())) {
                        nbPizza++;
                        total += pizza.getPrix();
                    }
                }
            }

            beneficeParCategorieDto.setNom(categorie.getNom());
            beneficeParCategorieDto.setNbPizzaVendu(nbPizza);
            beneficeParCategorieDto.setTotal(Math.round(total / 100));

            beneficeParCategorieDtos.add(beneficeParCategorieDto);
        }

        modelAndView.addObject("lignes" , beneficeParCategorieDtos);
        return modelAndView;
    }
}
