package pizza.spring.controleur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pizza.spring.dto.CommandeDto;
import pizza.spring.modele.Commande;
import pizza.spring.modele.Pizza;
import pizza.spring.service.CommandeService;
import pizza.spring.service.PizzaService;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
public class CommandeControleur {
    @Autowired
    private PizzaService pizzaService;

    @Autowired
    private CommandeService commandeService;

    @GetMapping({"/commande"})
    public ModelAndView commandeList(ModelMap modelMap) {
        ModelAndView modelAndView = new ModelAndView("commande/list", "commande", new ArrayList<Commande>());
        modelAndView.addObject("listeCommandes", commandeService.getAllCommandeEnAttente());
        return modelAndView;
    }

    @PostMapping({"/commande"})
    public ModelAndView finished(ModelMap modelMap, @RequestParam("commandeId") long commandeId) {
        commandeService.commandeFinished(commandeId);
        ModelAndView modelAndView = new ModelAndView("commande/list", "commande", new ArrayList<Commande>());
        modelAndView.addObject("listeCommandes", commandeService.getAllCommandeEnAttente());
        return modelAndView;
    }

    @GetMapping({"/commande/suivante"})
    public ModelAndView nextCommande(ModelMap modelMap) {
        ModelAndView modelAndView = new ModelAndView("commande/suivante", "commande", new Commande());
        modelAndView.addObject("commande", commandeService.getFirstCommande());
        return modelAndView;
    }

    @GetMapping({"/commande/formulaire"})
    public ModelAndView formulaire(ModelMap modelMap){
        ModelAndView modelAndView = new ModelAndView("commande/formulaire", "commandeDto", new CommandeDto());
        modelAndView.addObject("listePizzas", pizzaService.getListePizza());
        return modelAndView;
    }

    @PostMapping({"/commande/formulaire"})
    public String create(@ModelAttribute("commandeDto") CommandeDto commandeDto, ModelMap model){
        Commande commande = new Commande();
        commande.setDateEmission(new Timestamp(System.currentTimeMillis()).toString());
        commande.setEnAttente(true);

        List<Pizza> pizzas = new ArrayList<>();
        for (long id: commandeDto.getIdPizzas()) {
            pizzas.add(pizzaService.getById(id));
        }

        commande.setNom(commandeDto.getNom());
        commande.setTelephone(commandeDto.getTelephone());
        commande.setPizzas(pizzas);

        commandeService.save(commande);

        HashMap<String, String> map = new HashMap<>();
        map.put("nom", commande.getNom());
        map.put("telephone", commande.getTelephone());
        map.put("dateEmission", commande.getDateEmission());
        model.addAllAttributes(map);

        return "commande/confirmation";
    }

    @GetMapping({"/processus"})
    public String processus(ModelMap model) {
        return null;
    }
}
