package pizza.spring.dto;

public class BeneficeParCategorieDto {
    private String nom;
    private int nbPizzaVendu;
    private int total;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getNbPizzaVendu() {
        return nbPizzaVendu;
    }

    public void setNbPizzaVendu(int nbPizzaVendu) {
        this.nbPizzaVendu = nbPizzaVendu;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
