package pizza.spring.dto;

import java.util.List;

public class CommandeDto {
    private String nom;
    private String telephone;
    private List<Integer> idPizzas;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public List<Integer> getIdPizzas() {
        return idPizzas;
    }

    public void setIdPizzas(List<Integer> idPizzas) {
        this.idPizzas = idPizzas;
    }
}
