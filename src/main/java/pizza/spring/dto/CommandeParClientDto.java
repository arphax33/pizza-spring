package pizza.spring.dto;

public class CommandeParClientDto {
    private String nom;
    private String nbCommande;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNbCommande() {
        return nbCommande;
    }

    public void setNbCommande(String nbCommande) {
        this.nbCommande = nbCommande;
    }
}
